/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/21
* TITLE: De celsius a Fahrenheit
*/
import java.util.Scanner
//importo libreria de java (ver en un futuro)
fun main(args: Array<String>) {

    print("Introduzca grados celsius (decimal): ")
    val escaneo = Scanner(System.`in`)
    // Scanner(System.`in`) he llamado a un libro de la libreria de java (verse en un futuro)
    val celsius = escaneo.nextDouble()
    //variable escaneo usa el "poder" "NEXT.INT()"
    val fahrenheit = celsius * 1.8 + 32
    print("La temperatura en fahrenheit es: ")
    print(fahrenheit)
    print("ºF")

}