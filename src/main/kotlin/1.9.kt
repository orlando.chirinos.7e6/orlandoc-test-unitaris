import java.util.Scanner
/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/21
* TITLE: Calcula el descuento
*/
fun main() {

    print("Introduce precio del primer producto: ")
    val escaneo = Scanner(System.`in`)
    val read = escaneo.nextDouble()

    print("Introduce precio del primer producto pero con su descuento: ")
    val read2 = escaneo.nextDouble()

    print("El descuento aplicado es: ${discount(read,read2)}%")
}
fun discount(read: Double, read2: Double): Double {
    val result = read - read2
    return ((result/ read)*100)

}