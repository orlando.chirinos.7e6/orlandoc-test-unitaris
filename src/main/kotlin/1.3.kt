import java.util.Scanner
/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/19
* TITLE: Suma de dos números enteros
*/
fun main() {
    print("Introduce primer número a sumar: ")
    val escaneo = Scanner(System.`in`)
    val inputvalue = escaneo.nextInt().toLong()

    print("Introduce segundo número a sumar: ")
    val inputvalue2 = escaneo.nextInt().toLong()

    print("Número después de ser sumado es: ${suma(inputvalue, inputvalue2)}")

}
fun suma(inputvalue: Long, inputvalue2: Long): Long {
    return inputvalue + inputvalue2
}