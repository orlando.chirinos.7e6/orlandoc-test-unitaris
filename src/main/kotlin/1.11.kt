import java.util.Scanner
/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/21
* TITLE: Calculadora de volumen de aire
*/
fun main() {
    val escaneo = Scanner(System.`in`)

    print("Introduce el largo de la habitación (metros y decimal): ")
    val length = escaneo.nextDouble()

    print("Introduce el amplio de la habitación (metros y decimal): ")
    val wide = escaneo.nextDouble()

    print("Introduce la altura de la habitación(metros y decimal): ")
    val height = escaneo.nextDouble()

    print("El volúmen de la habitación es: ${volumevalue(length,wide,height)}")

}
fun volumevalue(length: Double, wide: Double, height: Double): Double {
    val volume = length * wide * height
    return volume
}