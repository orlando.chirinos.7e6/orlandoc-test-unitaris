import java.util.*
/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/21
* TITLE: Número siguiente
*/
fun main() {

    print("Introduzca número entero: ")
    val escaneo = Scanner(System.`in`)
    val read = escaneo.nextInt().toLong()

    print("Después va el: ${nextnumber(read)}")
}
fun nextnumber(read: Long): Long {
    return read + 1
}