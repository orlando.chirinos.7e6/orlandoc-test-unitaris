import java.util.*
/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/21
* TITLE: ¿Qué temperatura hace?
*/
fun main() {

    print("Introduzca temperatura original (celsius): ")
    val escaneo = Scanner(System.`in`)
    val temperature = escaneo.nextDouble()

    print("Introduzca aumento de temperatura (celsius): ")
    val increase = escaneo.nextDouble()

    print("La temperatura actual es: ${temp(temperature,increase)} ºC")

}
fun temp(temperature: Double, increase: Double): Double {
    val total = temperature + increase
    return total
}