import java.util.Scanner
/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/21
* TITLE: ¿Cual es la medida de mi pizza?
*/
fun main() {

    print("Introduce el diametro de la pizza: ")
    val escaneo = Scanner(System.`in`)
    val diameter = escaneo.nextDouble()

    print("El área de la pizza es: ${pizzaarea(diameter)} cm2")
}
fun pizzaarea(diameter: Double): Double {
    val ratio = diameter / 2
    val surface = Math.PI * (ratio * ratio)

    return surface
}