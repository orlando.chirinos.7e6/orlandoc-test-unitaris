import java.util.Scanner
/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/19
* TITLE: Operación loca
*/

fun main() {

    val escaneo = Scanner(System.`in`)
    print("Introduce primer valor: ")
    val inputvalue = escaneo.nextInt()

    print("Introduce segundo valor: ")
    val inputvalue2 = escaneo.nextInt()

    print("Introduce tercer valor: ")
    val inputvalue3 = escaneo.nextInt()

    print("Introduce cuarto valor: ")
    val inputvalue4 = escaneo.nextInt()

    print("Resultado de la operación: ${randomoperation(inputvalue,inputvalue2,inputvalue3,inputvalue4)}")

}
fun randomoperation(inputvalue: Int, inputvalue2: Int, inputvalue3: Int, inputvalue4: Int): Int {
    return ((inputvalue + inputvalue2) * (inputvalue3 % inputvalue4))
}