import java.util.Scanner
/*
 AUTHOR: Orlando David Chirinos Cisneros
 DATE: 2022/09/19
 TITLE: Dobla el entero
*/
fun main() {
    println("Introduce número a duplicar: ")

    val scan = Scanner(System.`in`)
    val inputvalue = scan.nextInt().toLong()

    println("Número después de ser duplicado es: ${duplicado(inputvalue)}")
}
fun duplicado(inputvalue: Long): Long {
    return inputvalue * 2
}