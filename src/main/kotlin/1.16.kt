import java.util.*
/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/21
* TITLE: Transforma el entero
*/
fun main() {
    val escaneo = Scanner(System.`in`)

    print("Introduzca entero: ")
    val entero = escaneo.nextInt()

    print("En decimal es: ${decimal(entero)}")
}

fun decimal(entero: Int): Double {
    val numtodecimal = entero.toDouble()
    return numtodecimal
}