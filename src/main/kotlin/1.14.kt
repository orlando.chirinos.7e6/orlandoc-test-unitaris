import java.util.*
/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/21
* TITLE: Divisor de cuenta
*/
fun main() {

    print("Introduzca número de conmensales: ")
    val escaneo = Scanner(System.`in`)
    val client = escaneo.nextDouble()

    print("Introduzca precio de la cuenta: ")
    val price = escaneo.nextDouble()

    print("Cada comensal debe pagar: ${dinerocuenta(client,price)}€")

}

fun dinerocuenta(client: Double, price: Double): Double {
    val total = price / client
    return total
}