import java.util.Scanner
/*
* AUTHOR: Orlando David Chirinos Cisneros
* DATE: 2022/09/21
* TITLE: Dobla el decimal
*/
fun main() {

    print("Introduzca número decimal: ")
    val escaneo = Scanner(System.`in`)
    val read = escaneo.nextDouble()

    print("Después va el: ${doubledecimal(read)}")

}
fun doubledecimal(read: Double): Double {
    return read * 2
}