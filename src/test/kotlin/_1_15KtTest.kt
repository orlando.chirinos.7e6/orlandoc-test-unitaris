import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class _1_15KtTest{

    @Test
    fun bignums(){
        val expected = 29
        assertEquals(expected, nextsecond(998887648))
    }

    @Test
    fun bignegnum(){
        val expected = -37
        assertEquals(expected, nextsecond(-9797498))
    }

    @Test
    fun negnums(){
        val expected = -30
        assertEquals(expected, nextsecond(-31))
    }

}