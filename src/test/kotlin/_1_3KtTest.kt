import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class _1_3KtTest{

    @Test
    fun sumnegativenumber(){
        val expected = -350L
        assertEquals(expected, suma(-200, -150))
    }

    @Test
    fun sumAnegative(){
        val expected = -350L
        assertEquals(expected, suma(-400, 50))
    }

    @Test
    fun sumbignumber(){
        val expected = 217767110479
        assertEquals(expected, suma(215612333345, 2154777134))
    }
}