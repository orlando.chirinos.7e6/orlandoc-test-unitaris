import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class _1_13KtTest{

    @Test
    fun negnums(){
        val expected = -22.0
        assertEquals(expected, temp(-10.0, -12.0))
    }

    @Test
    fun negonenum(){
        val expected = -16.0
        assertEquals(expected, temp(-37.0, 21.0))
    }

    @Test
    fun bignums(){
        val expected = 1.11111111E9
        assertEquals(expected, temp(123456789.0, 987654321.0))
    }
}