import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class _1_8KtTest{
    @Test
    fun numberzero(){
        val expected = 0.0
        assertEquals(expected, doubledecimal(0.0))
    }

    @Test
    fun negativenumber(){
        val expected = -146.2
        assertEquals(expected, doubledecimal(-73.1))
    }

    @Test
    fun bignumbers(){
        val expected = 1.9998585858E10
        assertEquals(expected, doubledecimal(9999292929.0))
    }
}