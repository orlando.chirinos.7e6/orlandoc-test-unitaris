import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class _1_6KtTest{
    @Test
    fun negativenumbers(){
        val expected = -18L
        assertEquals(expected, pupitres(-20, -12, -7))
    }
    @Test
    fun zero(){
        val expected = 1L
        assertEquals(expected, pupitres(0, 0, 0))
    }

    @Test
    fun bignums(){
        val expected = 1222222L
        assertEquals(expected, pupitres(777777, 666666, 999999))
    }
}