import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class _1_9KtTest{
    @Test
    fun negnum(){
        val expected = 66.62887629209737
        assertEquals(expected, discount(-2999.0, -1000.8))
    }

    @Test
    fun bignums(){
        val expected = 99.99949956134049
        assertEquals(expected, discount(99912345.0, 500.0))
    }

    @Test
    fun negpositive(){
        val expected = 119.98021958261322
        assertEquals(expected, discount(1000.99, -200.0))
    }


}