import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class _1_4KtTest{

    @Test
    fun multiplynegativenumbers(){
        val expected = 25L
        assertEquals(expected, calcarea(-5, -5))
    }

    @Test
    fun multiplybignumbers(){
        val expected = 777776222223L
        assertEquals(expected, calcarea(999999, 777777))
    }

    @Test
    fun multiplybyzero(){
        val expected = 0L
        assertEquals(expected, calcarea(0, 9))
    }
}