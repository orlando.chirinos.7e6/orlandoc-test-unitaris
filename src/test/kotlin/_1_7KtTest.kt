import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class _1_7KtTest{
    @Test
    fun negnum(){
        val expected = -5L
        assertEquals(expected, nextnumber(-6))
    }

    @Test
    fun bignumber(){
        val expected = 12345679L
        assertEquals(expected, nextnumber(12345678))
    }

    @Test
    fun negbignum(){
        val expected = -666666665L
        assertEquals(expected, nextnumber(-666666666))
    }
}