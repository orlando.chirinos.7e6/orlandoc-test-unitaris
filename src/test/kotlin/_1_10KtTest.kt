import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class _1_10KtTest{

    @Test
    fun negnum(){
        val expected = 318.8893257980404
        assertEquals(expected, pizzaarea(-20.15))
    }

    @Test
    fun numzero(){
        val expected = 0.04908738521234052
        assertEquals(expected, pizzaarea(0.25))
    }

    @Test
    fun bignum(){
        val expected = 7.751760456082222E8
        assertEquals(expected, pizzaarea(31416.31416))
    }
}