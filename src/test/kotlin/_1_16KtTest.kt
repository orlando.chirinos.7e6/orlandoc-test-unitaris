import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class _1_16KtTest{

    @Test
    fun numzero(){
        val expected = 0.0
        assertEquals(expected, decimal(0))
    }
    @Test
    fun bignum(){
        val expected = 12345678.0
        assertEquals(expected, decimal(12345678))
    }
    @Test
    fun negnum(){
        val expected = -991.0
        assertEquals(expected, decimal(-991))
    }
}