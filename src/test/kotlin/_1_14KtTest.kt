import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class _1_14KtTest{

    @Test
    fun negnums(){
        val expected = -0.8888888888888888
        assertEquals(expected, dinerocuenta(-90.0, 80.0))
    }

    @Test
    fun bignums(){
        val expected = 0.006278642732641955
        assertEquals(expected, dinerocuenta(1576562869.0, 9898675.0))
    }

    @Test
    fun samenumber(){
        val expected = 1.0
        assertEquals(expected, dinerocuenta(35.0, 35.0))
    }
}