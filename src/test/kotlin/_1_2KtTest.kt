import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class _1_2KtTest{

    @Test
    fun multiplyminusnumber(){
        val expected = -1348L
        assertEquals(expected, duplicado(-674))
    }

    @Test
    fun multiplylongnumber(){
        val expected = 956302557144L
        assertEquals(expected, duplicado(478151278572))
    }

    @Test
    fun multiplysmolnumber(){
        val expected = 100L
        assertEquals(expected, duplicado(50))
    }
}