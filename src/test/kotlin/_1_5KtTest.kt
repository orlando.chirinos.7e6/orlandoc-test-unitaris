import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class _1_5KtTest{

    @Test
    fun negativenumbers(){
        val expected = 0
        assertEquals(expected, randomoperation(-6, -2, -10, -1))
    }

    @Test
    fun zeronumbers(){
        val expected = 0
        assertEquals(expected, randomoperation(-20, 0, 0, 13))
    }

    @Test
    fun bignumbers(){
        val expected = 1989689512
        assertEquals(expected, randomoperation(534745234, 5438792, 43583745, 234565))
    }
}