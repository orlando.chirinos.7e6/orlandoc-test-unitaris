import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class _1_11KtTest{
    @Test
    fun negnum(){
        val expected = -781.9999999999999
        assertEquals(expected, volumevalue(-8.5, -10.0, -9.2))
    }

    @Test
    fun samenum(){
        val expected = 50653.0
        assertEquals(expected, volumevalue(37.0, 37.0, 37.0))
    }

    @Test
    fun bignum(){
        val expected = 1.1078650627612048E14
        assertEquals(expected, volumevalue(9999.12, 11177.73, 991223.23))
    }
}